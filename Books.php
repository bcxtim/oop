<?php

require_once('ActiveRecord.php');

/**
 * Created by PhpStorm.
 * User: root
 * Date: 27.12.15
 * Time: 17:47
 */
class Books extends ActiveRecord
{

    public $author;
    public $title;
    public $text;
    public $image_name;
    public $table = 'books';


    /**
     * Выбор всех книг, и загоняем их в ассоциативный массив
     * @return mixed
     */

    public function getAllBooks()
    {

        $query = $this->selectAll('*');
        $books = $this->fetchArrayAll($query);

        return $books;
    }


    /** Вывод одной книги по ID
     * @param $id
     * @return array|null
     */

    public function getOneBookById($id)
    {
        $book_id = (int)$id;
        $query = $this->selectAllById('*', $book_id);
        $book = $this->fetchArrayOne($query);

        return $book;
    }

    /**
     * Создание новой книги
     * @param $author
     * @param $title
     * @param $text
     * @param $image_name
     * @return bool|mysqli_result
     */

    public function createBook($author, $title, $text, $image_name)
    {
        $this->author = mysqli_real_escape_string($this->getLink(), $author);
        $this->title = mysqli_real_escape_string($this->getLink(), $title);
        $this->text = mysqli_real_escape_string($this->getLink(), $text);
        $this->image_name = mysqli_real_escape_string($this->getLink(), $image_name);
        $query = $this->create(['author', 'title', 'text','image'],
            [$this->author, $this->title, $this->text, $this->image_name]);
        $this->uploadFile($image_name);
        $book_id = mysqli_insert_id($this->getLink());

        return $book_id;
    }

    /**
     * Удалить книгу по айди
     * @param $id
     * @return bool|mysqli_result
     */
    public function deleteBookById($id)
    {
        $book_id = (int)$id;
        $result = $this->deleteById($book_id);
        return $result;
    }

    /**
     * Редактирование книги по айди
     * @param $author
     * @param $title
     * @param $text
     * @param $id
     * @return bool|mysqli_result
     */

    public function editBookById($author, $title, $text, $id)
    {
        $this->author = mysqli_real_escape_string($this->getLink(), $author);
        $this->title = mysqli_real_escape_string($this->getLink(), $title);
        $this->text = mysqli_real_escape_string($this->getLink(), $text);
        $book_id = (int)$id;
        $result = $this->update(['author' => $this->author, 'title' => $this->title, 'text' => $this->text], $book_id);

        return $result;
    }

    /**
     * Загрузка картинки книги
     * @param $image_name
     * @return bool
     */

    public function uploadFile($image_name)
    {
        $this->image_name = mysqli_real_escape_string($this->getLink(),$image_name);
        $image = move_uploaded_file($_FILES['upload-file']['tmp_name'], "images/".$image_name);
        return $image;
    }




}