<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.12.15
 * Time: 20:39
 */

require_once('Books.php');
require_once('header.php');

if(isset($_POST['create-book']))
{
    $author = $_POST['author'];
    $title = $_POST['title'];
    $text = $_POST['text'];

    $books = new Books();
    $image_name = $_FILES['upload-file']['name'];

    $book_id = $books->createBook($author, $title, $text, $image_name);
    header("Location: book.php?id=".$book_id);
}

?>

<form action="" method="post" enctype="multipart/form-data">
    <label>Автор</label>
    <input type="text" name="author" />  <br /> <br />
    <label>Название книги</label>
    <input type="text" name="title" /> <br /> <br />
    <label>Автор</label>
    <textarea rows="20" cols="40" name="text"></textarea> <br /> <br />
    <input type="file" name="upload-file" /> <br /><br />
    <input type="submit" name="create-book" value="Добавить книгу">

</form>
