<?php
require_once('Db.php');

/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.01.16
 * Time: 18:00
 */
class DBQuery extends Db
{

    /**
     * Выбрать все из таблицы
     * @param $columns
     * @param $table
     * @return bool|mysqli_result
     */
    public function selectAll($columns, $table)
    {
        if(is_array($columns))
        {
            $columns = implode(', ', $columns);
        }
        $query = "SELECT $columns FROM $table";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Выбор всех результатов по запросу
     * @param $query
     * @return array
     */
    public function fetchArrayAll($query)
    {
        $rows = array();
        while($row = mysqli_fetch_assoc($query))
        {
            $rows[$row['id']] = $row;
        }
        return $rows;
    }

    /**
     * Выбрать все из таблицы по ID
     * @param $columns
     * @param $table
     * @param $id
     * @return bool|mysqli_result
     */
    public function selectAllById($columns, $table, $id)
    {
        $query = "SELECT $columns FROM $table WHERE id = '$id'";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Вывод одного результата в ассоциативном массиве
     * @param $query
     * @return array|null
     */

    public function  fetchArrayOne($query)
    {
        $row = mysqli_fetch_assoc($query);

        return $row;
    }



    /**
     * Удалить из таблицы строку по ID
     * @param $table
     * @param $id
     * @return bool|mysqli_result
     */
    public function deleteById($table, $id)
    {
        $query = "DELETE FROM $table WHERE id = $id";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Создать новую запись в таблице
     * @param $table
     * @param $columns
     * @param $params
     * @return bool|mysqli_result
     */
    public function create($table, $columns, $params)
    {

        $values = [];
        foreach($params as $value)
        {
            $values[] = "'".$value."'";
        }
        $query = "INSERT INTO $table (" . implode(', ', $columns) . ") VALUES (" . implode(', ', $values) . ")";
        $result = $this->query($query);
        return $result;
    }

    /**
     * Обновить в таблице по ID
     * @param $table
     * @param $params
     * @param $id
     * @return bool|mysqli_result
     */

    public function update($table, $params, $id)
    {
        $setParts = [];

        foreach($params as $columns => $value)
        {
            $setParts[] = $columns . ' = "'.$value.'"';

        }
        $query = "UPDATE $table SET ". implode(', ', $setParts)." WHERE id = $id";
        $result = $this->query($query);
        return $result;
    }
}