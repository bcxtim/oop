<?php
require_once('DBQuery.php');
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.01.16
 * Time: 12:50
 */
class ActiveRecord extends DBQuery
{
    public $table;

    /**
     * Выбрать все значения
     * @param $columns
     * @return bool|mysqli_result
     */
    public function selectAll($columns)
    {
        $result = parent::selectAll($columns, $this->table);
        return $result;
    }

    /**
     * Выбрать все значения по ID
     * @param $columns
     * @param $id
     * @return bool|mysqli_result
     */
    public function selectAllById($columns, $id)
    {
        $result = parent::selectAllById($columns, $this->table, $id);
        return $result;
    }

    /**
     * Удалить строку по ID
     * @param $id
     * @return bool|mysqli_result
     */
    public function deleteById($id)
    {
        $result = parent::deleteById($this->table, $id);
        return $result;
    }

    /**
     * Создание новой записи в БД
     * @param $columns
     * @param $params
     * @return bool|mysqli_result
     */

    public function create($columns, $params)
    {
        $result = parent::create($this->table, $columns, $params);
        return $result;
    }

    /**
     * Обновить в таблице
     * @param $params
     * @param $id
     * @return bool|mysqli_result
     */
    public function update($params, $id)
    {
        $result = parent::update($this->table, $params, $id);
        return $result;
    }
}