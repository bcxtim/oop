<?php
require_once('Books.php');
require_once('header.php');

$books = new Books();
$result = $books->getAllBooks();

if($_GET['action'] == "delete")
{
    $result = $books->deleteBookById($_GET['id']);
    header("Location: index.php");
} elseif($_GET['action'] == 'edit')
{
    header("Location: book.php?action=edit&id=".$_GET['id']);
}

?>


<a href="add-book.php">Добавить книгу</a>
<table border="1">
    <tr>
        <th>Изображение</th>
        <th>Автор</th>
        <th>Название</th>
        <th>Текст</th>
        <th>Редактировать</th>
        <th>Удалить</th>
    </tr>
    <?php foreach($result as $book): ?>
    <tr>
        <td><a href="book.php?id=<?= $book['id']; ?>">
                <img src="images/<?= $book['image']; ?>" height="150" width="150" alt="<?= $book['title']; ?>" >
            </a>
        </td>
        <td> <?= $book['author']; ?></td>
        <td><?= $book['title']; ?></td>
        <td><?= $book['text']; ?></td>
        <td><a href="index.php?action=edit&id=<?= $book['id']; ?>">Редактировать книгу</a> </td>
        <td><a href="index.php?action=delete&id=<?= $book['id']; ?>">Удалить книгу</a> </td>
    </tr>
    <?php endforeach; ?>
</table>



<?php
require_once('footer.php');

