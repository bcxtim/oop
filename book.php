<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.12.15
 * Time: 13:10
 */

require_once('Books.php');
require_once('header.php');
$id = $_GET['id'];

$books = new Books();
$book = $books->getOneBookById($id);
if(isset($_POST['update-book']))
{
    $author = $_POST['author'];
    $title = $_POST['title'];
    $text = $_POST['text'];

    $result = $books->editBookById($author, $title, $text, $id);
    header("Location: book.php?id=".$id);
}

?>

<?php if($_GET['action'] == 'edit'): ?>
    <form action="" method="post">
        <label>Автор</label>
        <input type="text" name="author" value="<?= $book['author']; ?>" />  <br /> <br />
        <label>Название книги</label>
        <input type="text" name="title" value="<?= $book['title']; ?>" /> <br /> <br />
        <label>Автор</label>
        <textarea rows="20" cols="40" name="text"><?= $book['text']; ?></textarea> <br /> <br />
        <input type="submit" name="update-book" value="Обновить книгу">

    </form>
    <?php else: ?>

<table border="1">
    <tr>
        <th>Автор</th>
        <th>Название</th>
        <th>Текст</th>
        <th>Изображение</th>
    </tr>
        <tr>
            <td><?= $book['author']; ?></td>
            <td><?= $book['title']; ?></td>
            <td><?= $book['text']; ?></td>
            <td><img src="images/<?= $book['image']; ?>" height="150" width="150" alt="<?= $book['title']; ?>" > </td>
        </tr>
</table>

    <?php endif; ?>


<?php

require_once('footer.php');