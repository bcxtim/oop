<?php

class Db {


    //параметры подключения к БД
    private $host = 'localhost';
    private $username = 'root';
    private $password = '13123974';
    private $database = 'oop';

    private $link = null;

    /**
     * Подключение к БД
     */
    public function __construct()
    {
        $host = $this->host;
        $username = $this->username;
        $password = $this->password;
        $database = $this->database;

        $this->link = mysqli_connect($host, $username, $password, $database);
        mysqli_query($this->link, "SET NAMES utf8");

    }

    /**
     * Возвращение соединения к БД
     * @return mysqli|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Выполнение запроса и экранирование
     * @param null $query
     * @return bool|mysqli_result
     */
    public function query($query = null)
    {
        return mysqli_query($this->link, $query);
    }

}